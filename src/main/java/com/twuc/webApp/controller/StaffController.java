package com.twuc.webApp.controller;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import com.twuc.webApp.domain.StaffZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.net.URI;
import java.time.ZoneId;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Optional;

@RestController
public class StaffController {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    EntityManager em;
    @PostMapping("/api/staffs")
    public ResponseEntity addAStaff(@RequestBody Staff staff) {
        if (staff.getFirstName() == null || staff.getLastName() == null) {
            return ResponseEntity.badRequest().build();
        }
        staffRepository.save(staff);
        staffRepository.flush();
        return ResponseEntity.created(URI.create("/api/staffs/"+staff.getId())).build();
    }

    @GetMapping("/api/staffs/{staffId}")
    public ResponseEntity findStaff(@PathVariable Long staffId) {
        Optional<Staff> optionalStaff = staffRepository.findById(staffId);
        if (!optionalStaff.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok().body(optionalStaff.get());
    }
    @GetMapping("/api/staffs")
    public  ResponseEntity findAllStaffs() {
        List<Staff> all = staffRepository.findAll(Sort.by(Sort.Order.asc("id")));
        return  ResponseEntity.ok().body(all);
    }
    @PutMapping("/api/staffs/{staffId}/timezone")
    public ResponseEntity getStaffZoneIdById(@RequestBody StaffZone staffZone,
                                             @PathVariable Long staffId) {
        Staff staff = staffRepository.findById(staffId).get();
        staff.setZoneId(staffZone.getZoneId());
        staffRepository.save(staff);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/api/timezones")
    public ResponseEntity getTimezonesList() {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(ZoneRulesProvider.getAvailableZoneIds());
    }



}
