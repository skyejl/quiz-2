package com.twuc.webApp.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StaffZone {
    @Column
    private String zoneId;

    public StaffZone() {
    }

    public StaffZone(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
