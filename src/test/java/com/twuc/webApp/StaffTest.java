package com.twuc.webApp;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffZone;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StaffTest extends ApiTestBase {

    @Test
    void should_add_staff() throws Exception {
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Rob", "Hall"))))
                .andExpect(status().is(201));
    }

    @Test
    void should_be_bad_request_when_staff_argument_error() throws Exception {
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff(null, "Hall"))))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_particular_staff_when_get_by_id() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/api/staffs/" + 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_bad_request_when_not_find_staff_by_id() throws Exception {
        mockMvc.perform(get("/api/staffs/" + 12L))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_all_staffs() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)))
                .andExpect(status().isCreated());
        Staff secondStaff = new Staff("Rob2", "Hall2");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(secondStaff)))
                .andExpect(status().isCreated());
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
        .andExpect(jsonPath("$[0].id").value(1L))
        .andExpect(jsonPath("$[1].id").value(2L));
    }

    @Test
    void should_get_staff_zoneId() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)))
                .andExpect(status().isCreated());
        StaffZone staffZone = new StaffZone("Asia/Chongqing");
        mockMvc.perform(put("/api/staffs/"+1L+"/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staffZone)))
                .andExpect(status().is(200));
    }

    @Test
    void should_get_staff_information_contains_zonId() throws Exception {
        Staff staff = new Staff("Rob", "Hall", "Asia/Chongqing");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)))
                .andExpect(status().isCreated());
        mockMvc.perform(get("/api/staffs/"+1L))
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_get_timezone_list() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().is(200))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
