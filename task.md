story1:
AC1：
1. 生成staff实体
2. 通过post请求能够向数据库添加一条staff记录，返回的responseEntity应加入location header，并且status设置为201；
3. post 请求参数invalid，返回status应为400；
AC2:
1。通过get请求id为xx的staff记录，返回相应的
responseEntity；
2。get 请求失败，返回400；
AC3:
通过get请求返回所有的staff信息，并按顺序排序

story2:
AC1: 
1. staff table 增加一列zoneId;
2. 通过get请求id为xx的时区信息
AC2：
1. 通过get请求id为xx的staff用户信息，也返回带有时区的信息，时区可以为null

story3:
1. 用户可以请求`/api/timezones`API返回时区列表


